/**
 * This file is part of crustacea.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pub mod source;
pub mod gfx;

use std::collections::HashMap;

use sdl2::rect::{Rect, Point};

use self::source::Source;
use self::source::TextureBlending;

// ----------------------------------------------------------------------------

pub mod utils {
    use sdl2::rect::Point;

    pub struct XYSlices {
        vx: Vec<i16>,
        vy: Vec<i16>,
    }

    impl XYSlices {
        pub fn new(points: &Vec<Point>) -> XYSlices {
            let mut vx = Vec::new();
            let mut vy = Vec::new();

            for point in points {
                vx.push(point.x() as i16);
                vy.push(point.y() as i16);
            }
            XYSlices {
                vx: vx,
                vy: vy
            }
        }
        pub fn vx(&self) -> &[i16] {
            return &self.vx.as_slice();
        }

        pub fn vy(&self) -> &[i16] {
            return &self.vy.as_slice();
        }
    }
}

// ----------------------------------------------------------------------------

#[derive(Debug)]
pub struct Surface {
    id: String,
    source: Box<Source>,
    points: Vec<Point>,
    pub selected: bool,
}

impl Surface {
    pub fn new<T: Source + 'static>(id: String, source: T, 
        points: Vec<Point>) -> Surface {
        Surface {
            id: id,
            source: Box::new(source), 
            points: points,
            selected: false
        }
    }

    pub fn set_source<T: Source + 'static>(&mut self, source: T) {
        self.source = Box::new(source);
    }

    pub fn offset(&mut self, offset: &Point) {
        let mut new_points = Vec::new();
        for point in &self.points {
            new_points.push(point.offset(offset.x(), offset.y()));
        }
        self.points = new_points;
    }

    pub fn scale(&self) {

    }

    pub fn is_rect(&self) -> bool {
        if self.points.len() != 4 {
            return false;
        }
        // get points sorted by y-value, asc order
        // take first half and second, compare y-values of each pair
        let xy_slices = utils::XYSlices::new(&self.points);
        let vy = xy_slices.vy().clone();
        return true;
    }

    pub fn hits_plane(&self, point: &Point) -> bool {
        // https://wrf.ecse.rpi.edu//Research/Short_Notes/pnpoly.html
        let mut inside = false;
        let mut j = self.points().len() - 1;

        for mut i in 0..self.points().len() {
            let points = self.points();
            if (points[i].y() > point.y()) != (points[j].y() > point.y()) &&
                point.x() < (points[j].x() - points[i].x()) * (point.y() - points[i].y()) 
                / (points[j].y() - points[i].y()) + points[i].x()
            {
                inside = !inside;
            }
            i = i + 1;
            j = i;
        }
        return inside;
    }

    pub fn points(&self) -> &Vec<Point> {
        return &self.points;
    }
}

impl PartialEq for Surface {
    fn eq(&self, other: &Surface) -> bool {
        self.id == other.id
    }
}

// ----------------------------------------------------------------------------

pub struct Mapping {
    pub surfaces: Vec<Box<Surface>>,
}

impl Mapping { 
    pub fn new() -> Mapping {
        Mapping { surfaces: Vec::new() }
    }

    pub fn add_surface(&mut self, id: String, surface: Surface) {
        self.surfaces.push(Box::new(surface));
    }

    pub fn surfaces(&mut self) -> &mut Vec<Box<Surface>> {
        return &mut self.surfaces;
    }
}

//------------------------------------------------------------------------------

pub mod surface {
    use super::Surface;
    use super::Point;

    pub fn deselect_all(surfaces: &mut Vec<Box<Surface>>) {
        for surface in surfaces {
            surface.selected = false;
        }
    }

    pub fn select_at(surfaces: &mut Vec<Box<Surface>>, point: &Point,
        hits_plane: &Fn(&Surface, &Point) -> bool) {

        let selected_surfaces = surfaces.into_iter()
            .filter(|surface| hits_plane(&surface, point));
        for surface in selected_surfaces {
            surface.selected = true;
        }
    }

    pub fn offset_selected(surfaces: &mut Vec<Box<Surface>>, offset: &Point) {
        for surface in surfaces {
            if surface.selected {
                surface.offset(offset);
            }
        }
    }
}

// ----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
    use super::Point;
    use super::Mapping;
    use super::Surface;
    use super::surface;
    use super::source::TextureBlending;

    #[test]
    fn test_simple_mapping() {
        let points = vec![Point::new(150, 0),
                          Point::new(0, 300),
                          Point::new(300, 300)];
        let surface = Surface::new(String::from("S1"), TextureBlending::new(), 
            points);
        let mut mapping = Mapping::new();

        // mapping.add_surface(String::from("surface1"), surface);
        mapping.surfaces.push(Box::new(surface));
        
        assert_eq!(1, mapping.surfaces.len());
    }

    #[test]
    fn test_hits_plane_triangle() {
        let points = vec![Point::new(150, 0),
                          Point::new(0, 300),
                          Point::new(300, 300)];
        let surface = Surface::new(String::from("S1"), TextureBlending::new(), 
            points);

        assert_eq!(false, surface::hits_plane(&surface, &Point::new(150, 0)));
        assert_eq!(false, surface::hits_plane(&surface, &Point::new(0, 300)));
        assert_eq!(false, surface::hits_plane(&surface, &Point::new(300, 300)));
        
        assert_eq!(true, surface::hits_plane(&surface, &Point::new(149, 0)));
        assert_eq!(true, surface::hits_plane(&surface, &Point::new(0, 299)));
        assert_eq!(true, surface::hits_plane(&surface, &Point::new(299, 299)));
    }

    #[test]
    fn test_hits_plane_quad() {
        let points = vec![Point::new(100, 100),
                          Point::new(200, 100),
                          Point::new(200, 200),
                          Point::new(100, 200)];
        let surface = Surface::new(String::from("S1"), TextureBlending::new(), 
            points);

        assert_eq!(false, surface::hits_plane(&surface, &Point::new(99, 99)));

        assert_eq!(true, surface::hits_plane(&surface, &Point::new(101, 101)));
    }
}
