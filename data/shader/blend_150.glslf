#version 150 core

#define SCREEN 0
#define DODGE 1
#define BURN 2
#define OVERLAY 3
#define MULTIPLY 4
#define ADD 5
#define DIVIDE 6
#define GRAIN_EXTRACT 7
#define GRAIN_MERGE 8

// grayscale
uniform sampler2D t_Tex0;
// rgba
uniform sampler2D t_Tex1;

uniform int i_Blend;

in vec2 v_Uv;
out vec4 Target0;

void main() {
    // we sample from both textures using the same uv coordinates. since our
    // lena image is grayscale, we only get the first component.
    vec3 tex0 = vec3(texture(t_Tex0, v_Uv).r);
    vec3 tex1 = texture(t_Tex1, v_Uv).rgb;

    vec3 result = vec3(0.0);

    // normally you'd have a shader program per technique, but for the sake of
    // simplicity we'll just branch on it here.
    switch (i_Blend) {
        case SCREEN:
            result = vec3(1.0) - ((vec3(1.0) - tex0) * (vec3(1.0) - tex1));
            break;
        case DODGE:
            result = tex0 / (vec3(1.0) - tex1);
            break;
        case BURN:
            result = vec3(1.0) - ((vec3(1.0) - tex0) / tex0);
            break;
        case OVERLAY:
            result = tex0 * (tex0 + (tex1 * 2) * (vec3(1.0) - tex0));
            break;
        case MULTIPLY:
            result = tex0 * tex1;
            break;
        case ADD:
            result = tex0 + tex1;
            break;
        case DIVIDE:
            result = tex0 / tex1;
            break;
        case GRAIN_EXTRACT:
            result = tex0 - tex1 + 0.5;
            break;
        case GRAIN_MERGE:
            result = tex0 + tex1 - 0.5;
            break;
    }

    Target0 = vec4(result, 1.0);
}
