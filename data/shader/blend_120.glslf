#version 120

#define SCREEN 0
#define DODGE 1
#define BURN 2
#define OVERLAY 3
#define MULTIPLY 4
#define ADD 5
#define DIVIDE 6
#define GRAIN_EXTRACT 7
#define GRAIN_MERGE 8

// grayscale
uniform sampler2D t_Tex0;
// rgba
uniform sampler2D t_Tex1;

uniform int i_Blend;

varying vec2 v_Uv;

void main() {
    // we sample from both textures using the same uv coordinates. since our
    // lena image is grayscale, we only get the first component.
    vec3 tex0 = vec3(texture2D(t_Tex0, v_Uv).r);
    vec3 tex1 = texture2D(t_Tex1, v_Uv).rgb;
   
    vec3 result = vec3(0.0);

    // normally you'd have a shader program per technique, but for the sake of
    // simplicity we'll just branch on it here.
    if (i_Blend == SCREEN) {
        result = vec3(1.0) - ((vec3(1.0) - tex0) * (vec3(1.0) - tex1));
    } else if (i_Blend == DODGE) {
        result = tex0 / (vec3(1.0) - tex1);
    } else if (i_Blend == BURN) {
        result = vec3(1.0) - ((vec3(1.0) - tex0) / tex0);
    } else if (i_Blend == OVERLAY) {
        result = tex0 * (tex0 + (tex1 * 2) * (vec3(1.0) - tex0));
    } else if (i_Blend == MULTIPLY) {
        result = tex0 * tex1;
    } else if (i_Blend == ADD) {
        result = tex0 + tex1;
    } else if (i_Blend == DIVIDE) {
        result = tex0 / tex1;
    } else if (i_Blend == GRAIN_EXTRACT) {
        result = tex0 - tex1 + 0.5;
    } else if (i_Blend == GRAIN_MERGE) {
        result = tex0 + tex1 - 0.5;
    }

    gl_FragColor = vec4(result, 1.0);
}
